This R script contains code for the analysis and figures in Nguyen, M and Veraart, A.E.D. (2018), [`Bridging between short-range and long-range dependence with mixed spatio-temporal Ornstein-Uhlenbeck processes'](https://www.tandfonline.com/doi/abs/10.1080/17442508.2018.1466886). 

Please do let me know if you encounter any errors.